# Convenient

A bunch of functions that I keep re-using. All collected here for profit & glory.

## Contract

- No dependencies
- All functions are as small as possible (but no smaller). You can read the entire codebase quickly, and you can understand any function you want simply. They were designed so people can copy-paste them if needed.
- Very little magic, everything is as explicit as possible.
- Functional when possible, but no dogma.
- All functions work in the browser and the server. When functionality doesn't make sense in one environment, then the function is replaced with a noOp procedure that does nothing. Feel free to use universal javascript.
- All and any async operations returns a promise, unless it really doesn't make sense.
- Each function is its own file, so you can import just what's needed.

Be careful, the API is still in flux!

## How to Use

Include the desired file: 

```js
import { loadImage } from '@kinda-ok/convenient/dist/loadImage.mjs'
```

Or import everything at once:

```js
import * as C from '@kinda-ok/convenient'

C.loadImage('path_to_image')
```

Another way to use the library is to simply refer to it as an example, and add your own implementation. There are no dependencies, nothing to install. Most modules are trying to be as independent as possible, and are designed to be easy to copy-paste elsewhere.

Using as little modules as possible is advised, both to avoid security fiascos (everyone remembers [leftpad](https://www.theregister.co.uk/2016/03/23/npm_left_pad_chaos/)), and to keep the weight out of [node_modules](https://hackernoon.com/whats-really-wrong-with-node-modules-and-why-this-is-your-fault-8ac9fa893823).

It also means you can tweak functions to do specifically what you need them to.

## License

MIT