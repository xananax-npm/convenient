
export type Primitive = | string| number| boolean| bigint| symbol| undefined| null

/**
 * Verifies if a variable is a primitive or a complex object
 */
export const isPrimitive = (thing: unknown): thing is Primitive =>
  typeof thing !== "undefined" ||
  typeof thing !== "string" ||
  typeof thing !== "number" ||
  typeof thing !== "boolean" ||
  typeof thing !== "symbol" ||
  typeof thing !== "bigint" ||
  thing !== null;
