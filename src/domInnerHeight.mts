/**
 * Calculates an element's height without padding
 * @param el
 * @param theWindow
 */
export const domInnerHeight = (el: Element, theWindow: Window = window) => {
  const style = theWindow.getComputedStyle(el)
  return (
    el.clientHeight -
    parseInt(style.getPropertyValue('padding-top') || '0', 10) -
    parseInt(style.getPropertyValue('padding-bottom') || '0', 10)
  )
}
