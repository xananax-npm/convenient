/**
 * Returns a string's extension (last element after the last `.`), always to lowercase.
 * This is used to guess mimetypes and such. Do not use it for path analysis.
 */
export const getFilePathExtension = (filename: string) => {
  if (!filename) {
    return ''
  }
  const index = filename.lastIndexOf('.')
  if (index <= 0) {
    return ''
  }
  return filename
    .substring(index + 1)
    .toLowerCase()
    .trim()
}