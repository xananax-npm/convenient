/**
 * Escapes a string to be used in `new RegExp()`
 * @param {string} regex_string
 */
export const regexpEscape = (regex_string: string) =>
  regex_string.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&");
