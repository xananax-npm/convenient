//@ts-check

/**
 * Shortcut for `Promise.resolve().then(<function>)`
 * @param fn any function
 * @param [value] an optional initial value to pass to the function
 * @returns an awaited function
 */
export const then = <T,A>(fn: (value?: A) => T, value: A): Promise<T> =>
  Promise.resolve(value).then(fn)
