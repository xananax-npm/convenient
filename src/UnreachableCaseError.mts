/**
 * Utility: use it at the end of switch statements to make sure all matches are covered.
 * Only useful in an editor that supports JSDOC strong typing.
 *
 * Use it like so
 * ```js
 * switch(data):
 *  case A:
 *    doThing();
 *    break;
 *  case B:
 *    doOtherThing();
 *    break;
 *  default:
 *    throw new UnreachableCaseError(state);
 * ```
 * It `data` may be more options, then `state` will be underlined with the error
 * ```
 * Argument of type 'T' is not assignable to parameter of type 'never'
 * ```
 * Where `T` is the type of `data`.
 * To remove the error, handle all cases.
 */
export class UnreachableCaseError extends Error {
  constructor(value: never) {
    super(`Unreachable case: ${JSON.stringify(value)}`)
  }
}
