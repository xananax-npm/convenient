
/**
 * Returns the first title content in the document, if there is one.
 */
export const getHTMLFirstTitleContents = (content: ParentNode = document) => {
  const firstTitleElement = content.querySelector("h1");
  if (firstTitleElement == null) {
    return "";
  }
  return firstTitleElement.textContent || "";
}
