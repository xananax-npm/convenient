
/**
 * Limited fetch request that retrieves only headers.
 * @param path
 */
export const fetchHeaders = async (path: string) => {
  const response = await fetch(path, {
    method: "HEAD",
    headers: {
      Range: "bytes=0-0",
      "X-HTTP-Method-Override": "HEAD",
    },
  });

  if (!response.ok) {
    throw new Error(`Failed loading file '${path}'`);
  }
  return response.headers;
};
