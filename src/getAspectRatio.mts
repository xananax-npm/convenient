
interface Size{
  width: number,
	height: number
}

interface AspectRatio extends Size{
	ratio:number
}


/**
 * Calculates an ideal ratio
 * @param initial the initial size
 * @param current the current size
 */
export const getAspectRatio = (initial: Size, current: Size): AspectRatio => {
  const ratioW = current.width / initial.width;
  const ratioH = current.height / initial.height;
  const ratio = Math.min(ratioW, ratioH);
  const width = initial.width * ratio;
  const height = initial.height * ratio;
  return { width, height, ratio };
}
