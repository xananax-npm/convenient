
const overflowRegex = /(scroll|auto)/;

export const getMostLikelyNode = (node? :HTMLElement) => {
	const rootElement = (node && node.ownerDocument || document)
	return rootElement.scrollingElement || rootElement.documentElement
}

	

/**
 * Gets the first scrolling parent of a given node
 */
export const getScrollParent = (node: HTMLElement) => {
  if (!node) {
    return getMostLikelyNode()
  }
  const excludeStaticParent = node.style.position === "absolute";
  let parent = node;
  while (parent) {
    if (!parent.parentNode) {
      return getMostLikelyNode(node)
    }
    const style = window.getComputedStyle(parent);
    const position = style.position;
    const overflow = style.overflow;
    const overflowX = style.overflowX
    const overflowY = style.overflowY;
    if (position === "static" && excludeStaticParent) {
      parent = parent.parentNode as HTMLElement
      continue;
    }
    if (
      overflow &&
      overflowRegex.test(overflow) &&
      overflowX &&
      overflowRegex.test(overflowX) &&
      overflowY &&
      overflowRegex.test(overflowY)
    ) {
      return parent;
    }
    parent = parent.parentNode as HTMLElement
  }
  return getMostLikelyNode(node)
};
