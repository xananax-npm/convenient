/**
 * checks if the passed argument is numeric.
 * A string containing only numbers is numeric.
 */
export const isNumeric = (numeric: unknown): numeric is number => {
  if (typeof numeric === "number" || typeof numeric === "string") {
    const n = parseFloat(numeric + "");
    return !isNaN(n) && isFinite(n);
  }
  return false;
};
