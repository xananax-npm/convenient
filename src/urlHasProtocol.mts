/**
 * Protocols checks to see if a url has a known protocol.
 */
export const knownProtocols = /** @type {const} */ ([
  "http",
  "https",
  "mailto",
  "tel",
  "ftp",
  "ftps",
  "ssh",
  "sftp",
  "ipfs",
  "dat",
]);

export const protocolRegex = new RegExp(`^${knownProtocols.join("|")}//`);

export type Protocol = (typeof knownProtocols)[number]


/**
 * Checks if an url begins with a known protocol
 */
export const urlHasProtocol = (url: string): Protocol | '' => {
  if (url == null) {
    return "";
  }
  if (url[0] === "/" && url[1] === "/") {
    return (location?.protocol ?? "http");
  }
  return ((url.match(protocolRegex) ?? [""])[0]);
};
