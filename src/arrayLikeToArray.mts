
/**
 * Transforms any list (NodeList, FileList, ...) to an array
 * @param listLike anything that is iterable
 */
export const arrayLikeToArray = <T,>(listLike: ArrayLike<T> | Iterable<T>): T[] =>
  Array.prototype.slice.call(listLike);
