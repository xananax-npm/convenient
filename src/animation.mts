import { tween } from './animation/tween.mjs'
import { interpolation, type InterpolationFunction } from './animation/interpolation.mjs'
import { easing, type EasingEquation } from './animation/easing.mjs'


export const animation = {
	tween, 
	interpolation, 
	easing 
}

export type {InterpolationFunction, EasingEquation }

