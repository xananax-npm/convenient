
export type QueriedElementsAsArray<T extends keyof HTMLElementTagNameMap> = HTMLElementTagNameMap[T][]

/**
 * A small utility to query elements in a parent and get back an array
 */
export const querySelectorAll = <K extends keyof HTMLElementTagNameMap>(parent: ParentNode, selector: K): QueriedElementsAsArray<K> => [
  ...Array.from(parent.querySelectorAll(selector)),
];
