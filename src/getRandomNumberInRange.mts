//@ts-check
/**
 * Returns a random number between min (inclusive) and max (inclusive)
 * @param max
 * @param [min]
 * @param [rand] defaults to javascript's `Math.random`
 */
export const getRandomNumberInRange = (max: number, min: number = 0, rand: () => number = Math.random) =>
  rand() * (max - min) + min;
