/**
 */
export const isNotNull = <T,>(value: unknown): value is NonNullable<T> => value !== null;
