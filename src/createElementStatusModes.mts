import { isDev } from "./isDev.mjs";
import { isEnvBrowser } from "./isEnvBrowser.mjs";

type AnyValidValue = number | null | undefined | false

const fake = {
  set(mode:AnyValidValue = false){},
  get(){return ""},
  is(){return false}
}

/**
 *
 * Creates exclusive states for an HTML element. These states are added as classes
 * and can be used to drive CSS changes.
 * This is similar to @see {@link createElementFlag}, but while flags are turned off or on,
 * modes are supposed to be exclusive.
 * @param allModes A string list of all possible modes. It's advised
 *                 to prepend (`state-` or `mode-` to each for clarity)
 * @param element The element to add the classes to. Defaults to the
 *                document's body
 */
export const createElementStatusModes = (
  allModes: string[],
  element: Element = document.body
) => {

  if(!element){
    if(isDev && isEnvBrowser){
      console.warn("No suitable element passed to createElementStatusModes")
    }
    return fake
  }

  /**
   * 
   * @param {any} mode 
   * @returns {mode is number}
   */
  const isValidIndex = (mode: any): mode is number =>
    typeof mode === "number" && mode >= 0 && mode < allModes.length;

  /**
   * Sets a status mode (class name) on the element.
   * Pass a falsy value to clear all modes
   */
  const set = (mode: AnyValidValue = false) => {
    mode = isValidIndex(mode) ? mode : -1;
    const modeClass = allModes[mode];
    if (modeClass && element.classList.contains(modeClass)) {
      return;
    }
    element.classList.remove(...allModes);
    element.classList.add(modeClass);
  };

  /**
   * Verifies which of the given classes is set.
   * @returns {string | undefined} the class if there is one
   */
  const get = (): string | undefined =>
    allModes.find((className) => element.classList.contains(className));

  const is = (state: number) =>
    isValidIndex(state) && document.body.classList.contains(allModes[state]);

  return { set, get, is };
}
