/** today's date as an ISO string */
export const today = new Date().toISOString().split('T')[0]
