import { awaitReduce } from "./awaitReduce.mjs";

type Functor<T> = (item:T, index: number, arr:T[])=>any

/**
 * Runs each item through the given functor and awaits each iteration.
 * Think of it like a slow `forEach()`.
 * @param sequence
 * @param functor
 */
export const awaitForEach = <T, F extends Functor<T>>(sequence: T[], functor: F) => 
  awaitReduce(
    sequence,
    (_previous, item, index, arr) => functor(item, index, arr),
    null,
  )