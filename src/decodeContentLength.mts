
/**
 * Does a best guess attempt at finding out the size of a file from a request headers.
 * To access headers, server must send CORS header
 * `Access-Control-Expose-Headers: content-encoding, content-length x-file-size`
 * server must send the custom `x-file-size` header if gzip or other content-encoding is used.
 */
export const decodeContentLength = (headers: Headers) => {
  const contentEncoding = headers.get("content-encoding");
  const contentLength =
    headers.get(contentEncoding ? "x-file-size" : "content-length") ||
    (headers.has("content-range") &&
      headers.get("content-range")?.split("/")[1]) ||
    "0";
  return parseInt(contentLength, 10);
}
