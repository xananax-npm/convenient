

export type Slug = string

export const TRIM = /^\s+|\s+$/g
export const INVALID = /[^a-z0-9 -_]/g
export const WHITESPACE = /\s+/g
export const DASHES = /-+/g

/**
 * Converts a string to a safe string,
 * suitable to be used as an url
 * or as an html element class/id
 *
 * This doesn't transform camelCase in camel_case
 */
export const toSlug = (str: string): Slug =>
  str
    .toLowerCase()
    .replace(TRIM, '')
    .replace(INVALID, '')
    .replace(WHITESPACE, '_')
    .replace(DASHES, '-')
