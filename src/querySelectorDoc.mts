//@ts-check
import { querySelectorAll, type QueriedElementsAsArray } from "./querySelectorAll.mjs";

/**
 * A small utility to query elements in the document and get back an array
 */
export const querySelectorDoc = <K extends keyof HTMLElementTagNameMap>(selector: K): QueriedElementsAsArray<K> =>
  querySelectorAll(document, selector);
