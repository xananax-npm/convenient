/**
 * Returns a stylesheet
 */
export const makeStyleSheet = (cssString: string) => {
  const stylesheet = new CSSStyleSheet();
  stylesheet.replaceSync(cssString);
  return stylesheet;
};

/**
 * Convenience literal to create DOM CSSStyleSheet instances
 */
export const css = (strings: TemplateStringsArray, ...substitutions: any[][]) => {
  const formattedString = strings.reduce(
    (acc, curr, index) => acc + curr + (substitutions[index] || ""),
    ""
  );
  return makeStyleSheet(formattedString);
};
