
/**
 * Calculates an element's width without padding.
 */
export const domInnerWidth = (el: Element, theWindow: Window = window) => {
  const style = theWindow.getComputedStyle(el)
  return (
    el.clientWidth -
    parseInt(style.getPropertyValue('padding-left') || '0', 10) -
    parseInt(style.getPropertyValue('padding-right') || '0', 10)
  )
}

