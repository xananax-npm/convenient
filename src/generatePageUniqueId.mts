//@ts-check
import { generateReasonablyUniqueUUID } from './generateReasonablyUniqueUUID.mjs'

/**
 * Returns an id that is guaranteed to not exist in the current loaded page.
 * Only usable in the browser, after the page has loaded.
 * Using it outside the browser or prior to loading will yield a pseudo-unique id,
 * but not guaranteed unique.
 * Ids are deterministic, so calling this function in the same order will always return
 * the same set of ids.
 * @see {uuid}
 * @param [prefix] any prefix you like, it helps discriminate ids
 * @returns
 */
export const generatePageUniqueId = (prefix: string = '') => {
  let id = prefix
  let limit = 99999
  while (
    typeof document !== 'undefined' &&
    document.getElementById((id = `${prefix}${generateReasonablyUniqueUUID()}`)) &&
    limit-- > 0
  );
  return id
}
