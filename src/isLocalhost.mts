//@ts-check
/** Returns true if the window global object exists and the domain is localhost of 127.0.0.1 */
export const isLocalhost =
  typeof window !== 'undefined' &&
  /^localhost|127.0.0.1/.test(window.location.hostname)
