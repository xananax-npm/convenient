//@ts-check
/**
 * @overload
 * @param {true} value
 * @returns {false}
 */
/**
 * @overload
 * @param {false} value
 * @returns {true}
 */
/**
 * @overload
 * @param {unknown} value
 * @returns {boolean}
 */

interface Not{
	(value: unknown): boolean
	(value: true): false
	(value: false): true
	(value: undefined): true
	(value: null): true
	(value: null): true
	(value: ''): true
	(value: any[]): false
	(value: {}): false
}

/**
 * Inverter.
 * Easier to read than "!"
 */
export const not = ((value: unknown) => !value) as Not


