//@ts-check
import { identity, type Identity } from "./identity.mjs";

type Arity1<ARG, RESULT> = (arg: ARG) => RESULT;

type Piped<ARG, RESULT> = (arg: ARG) => RESULT

interface PipeMaker {
	<A, B, C, D, E, F>(
		fn1: Arity1<A, B>,
		fn2: Arity1<B, C>,
		fn3: Arity1<C, D>,
		fn4: Arity1<D, E>,
		fn5: Arity1<E, F>
	): Piped<A, F>;
	<A, B, C, D, E>(
		fn1: Arity1<A, B>,
		fn2: Arity1<B, C>,
		fn3: Arity1<C, D>,
		fn4: Arity1<D, E>
	): Piped<A, E>;
	<A, B, C, D>(
		fn1: Arity1<A, B>,
		fn2: Arity1<B, C>,
		fn3: Arity1<C, D>
	): Piped<A, D>;
	<A, B, C>(
		fn1: Arity1<A, B>,
		fn2: Arity1<B, C>
	): Piped<A, C>;
	<A, B>(fn1: Arity1<A, B>): Piped<A, B>;
	<A, B>(
		...fns: [Piped<A, any>, ...Piped<any, any>[], Piped<any, B>]
	): Piped<A, B>;
	(): Identity;
}

/**
 * Chains functions together. Accepts promises and non-promises. For example:
 * ```js
 * const add = ( n:number ) => n+10
 * const multiply = ( n:number ) => n*100
 * const happy = ( n:string|number ) => `happy ${n}`
 * const birthday = ( s:string ) => `${s} birthday!`
 * const happyBirthday = pipe(add,multiply,happy,birthday)
 * console.log(happyBirthday(5)) // happy 150 birthday!
 * ```
 */
export const pipe = (<A, B>(
	...fns: [Piped<A, any>, ...Piped<any, any>[], Piped<any, B>]
): Piped<A, B> => {
	switch (fns.length) {
		case 0:
			//@ts-ignore
			return identityPromise;
		case 1:
			return pipe1(fns[0]);
		case 2:
			return pipe2(fns[0], fns[1]);
		case 3:
			return pipe3(fns[0], fns[1], fns[2]);
		case 4:
			return pipe4(fns[0], fns[1], fns[2], fns[3]);
		case 5:
			return pipe5(fns[0], fns[1], fns[2], fns[3], fns[4]);
		default:
			break;
	}
	return pipes(...fns);
}) as PipeMaker;

export const pipes =
	<ARG, RESULT>(
		...fns: [Piped<ARG, any>, ...Piped<any, any>[], Piped<any, RESULT>]
	): ((arg: ARG) => RESULT) =>
	(arg) =>
		fns.reduce((p, fn) => p.then(fn), Promise.resolve(arg)) as RESULT;

export const pipe1 =
	<A, B>(f: Arity1<A, B>): Piped<A, B> =>
	(a: A): B =>
		f(a)

export const pipe2 =
	<A, B, C>(
		g: Arity1<A, B>,
		f: Arity1<B, C>
	): Piped<A, C> =>
	(a: A): C => f(g(a))

export const pipe3 =
	<A, B, C, D>(
		h: Arity1<A, B>,
		g: Arity1<B, C>,
		f: Arity1<C, D>
	): Piped<A, D> =>
	(a: A): D => f(g(h(a)))

export const pipe4 =
	<A, B, C, D, E>(
		i: Arity1<A, B>,
		h: Arity1<B, C>,
		g: Arity1<C, D>,
		f: Arity1<D, E>
	): Piped<A, E> =>
	(a: A): E => f(g(h(i(a))))

export const pipe5 =
	<A, B, C, D, E, F>(
		j: Arity1<A, B>,
		i: Arity1<B, C>,
		h: Arity1<C, D>,
		g: Arity1<D, E>,
		f: Arity1<E, F>
	): Piped<A, F> =>
	(a: A): F => f(g(h(i(j(a)))))
