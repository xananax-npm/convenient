type Functor<T,R> = (previous:R, item:T, index: number, arr:T[]) => any

/**
 * Runs each item through the given functor and awaits each iteration.
 * Think of it like a slow `reduce()`.
 */
export const awaitReduce = async <T,R, F extends Functor<T,R>>(sequence: T[], functor: F, accumulator: R) => {
  for (let index = 0; index < sequence.length; index++) {
    const item = sequence[index]
    await functor(accumulator, item, index, sequence)
  }
  return accumulator
}