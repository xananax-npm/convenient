import { isEnvBrowser } from './isEnvBrowser.mjs'
import { noOp } from './noOp.mjs'

/**
 * If true, the browser supports passive events
 *
 * https://stackoverflow.com/questions/37721782/what-are-passive-event-listeners
 *
 * Problem: All modern browsers have a threaded scrolling feature to permit scrolling to run smoothly even when expensive JavaScript is running, but this optimization is partially defeated by the need to wait for the results of any touchstart and touchmove handlers, which may prevent the scroll entirely by calling preventDefault() on the event.
 *
 * Solution: `{passive: true}`
 *
 * Example of how to use passive events:
 * ```js
 * document.addEventListener("touchstart", (e) => {
 *  e.preventDefault(); // does nothing since the listener is passive
 * }, {passive: true})
 * ```
 */
let passive_events_supported = false

if (isEnvBrowser) {
  try {
    const opts = Object.defineProperty({}, 'passive', {
      get() {
        passive_events_supported = true
      }
    })
    window.addEventListener('test', noOp, opts)
  } catch (e) {
    //
  }
  window.removeEventListener('test', noOp)
}

const PassiveEventsSupported = passive_events_supported
export { passive_events_supported, PassiveEventsSupported }

