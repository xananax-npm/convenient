

type StateListener<T> = (state: T, oldState?: T) => void


/**
 * Exports a simple event dispatcher holding an immutable state
 * set the state with setState() to dispatch a change to listeners.
 * @param state any initial state
 */
export const makeStateHolder = <T,>(state: T) => {
  
  const eventsListeners: Set<StateListener<T>> = new Set();

  /**
   * Listens to any state change. Receives the new, and the previous
   * state, which allows comparisons
   * @param listener a function to receive state changes
   */
  const connect = (listener: StateListener<T>) => {
    eventsListeners.add(listener);
    return () => {
      eventsListeners.delete(listener);
    };
  };

  /**
   * State is updated, and an event dispatched to all
   * listeners.
   * The state passed will be shallowly merged with the
   * previous state. To replace, use `setState`
   * @param newState any partial part of the state
   */
  const mergeState = (newState: Partial<T>) => {
    if (newState === state) {
      return;
    }
    const oldState = state;
    state = { ...state, ...newState };
    eventsListeners.forEach((listener) => {
      listener(state, oldState);
    });
  };

  /**
   * State is updated, and an event dispatched to all
   * listeners.
   * The state passed will replace the previous state
   * @param newState any partial part of the state
   */
  const setState = (newState: T) => {
    if (newState === state) {
      return;
    }
    const oldState = state;
    state = newState;
    eventsListeners.forEach((listener) => {
      listener(state, oldState);
    });
  };

  /**
   * Returns the current state.
   */
  const getState = () => state;

  /**
   * removes all listeners
   */
  const clean = () => {
    eventsListeners.clear();
  };

  return { connect, setState, mergeState, clean, getState };
};
