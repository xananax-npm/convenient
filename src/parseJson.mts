
/**
 * Parses json data.
 * does not throw errors, and returns the default value
 * in case of error.
 * @param data
 * @param [defaultReturn] Defaults to an empty object
 */
export const parseJson = <T extends JsonObject = JsonRoot>(
  data: string,
  //@ts-ignore
  defaultReturn: T = {} as JsonRoot
): T => {
  if (typeof data !== 'string') {
    return defaultReturn
  }
  try {
    return JSON.parse(data)
  } catch (e) {
    return defaultReturn
  }
}