/**
 * used in looping functions to stop or break
 * the loop
 */
export const SKIP = Symbol('skip')
export const BREAK = Symbol('break')

export type SKIP = typeof SKIP
export type BREAK = typeof BREAK
export type LOOP_SYMBOL = SKIP | BREAK
