/**
 * Returns the hash part of the url, but only if it starts with a `/`
 * This allows regular hashes to continue to work
 * It reads also query parameters
 */
export const getCurrentHashUrl = () => {
  const [path, searchStr] = (
    window.location.hash[1] === "/" ? window.location.hash.slice(2) : ""
  ).split("?");
  const params = new URLSearchParams(searchStr);
  return { path, params };
};

/**
 * Returns true if `getCurrentHashUrl()` path is not an empty string
 */
export const hasCurrentHashUrl = () => getCurrentHashUrl().path !== "";
