export { utils } from './interpolation/utils.mjs'
import { bezier } from './interpolation/bezier.mjs'
import { catmullRom } from './interpolation/catmullRom.mjs'
import { linear } from './interpolation/linear.mjs'

export type InterpolationFunction = 
| typeof bezier
| typeof catmullRom
| typeof linear

export const interpolation =
  { bezier
  , catmullRom
  , linear
  }

