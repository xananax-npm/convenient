//@ts-check
import { back } from './easing/back.mjs'
import { bounce } from './easing/bounce.mjs'
import { circular } from './easing/circular.mjs'
import { cubic } from './easing/cubic.mjs'
import { elastic } from './easing/elastic.mjs'
import { exponential } from './easing/exponential.mjs'
import { linear } from './easing/linear.mjs'
import { quadratic } from './easing/quadratic.mjs'
import { quartic } from './easing/quartic.mjs'
import { quintic } from './easing/quintic.mjs'
import { sinusoidal } from './easing/sinusoidal.mjs'

export type EasingEquation = ( k: number) => number

export interface EasingStack{
	In: EasingEquation
	Out: EasingEquation
	InOut: EasingEquation
}

export const easing =
  { back
  , bounce
  , circular
  , cubic
  , elastic
  , exponential
  , linear
  , quadratic
  , quartic
  , quintic
  , sinusoidal
  }

