
export const In = 
  ( k: any ) => k

export const Out = In
export const InOut = In

export const linear = { In, Out, InOut }

