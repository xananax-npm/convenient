import { factorial } from './factorial.mjs'

export const bernstein =
  ( n: number
  , i: number
  , fc: (n: number) => number = factorial() 
  )  => 
  ( fc(n) / fc(i) / fc(n - i))

