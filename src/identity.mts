
/**
 * Returns the passed object
 */
export const identity = <T,>(value: T): T => value;

export type Identity = <T>(value: T) => T
