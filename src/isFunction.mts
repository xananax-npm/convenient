/**
 *
 * Returns true if the passed object is function
 *
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export const isFunction = (thing: unknown): thing is Function => typeof thing == "function";
