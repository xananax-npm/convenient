
/**
 * If true, the passed argument is a string
 */
export const isString = (obj: unknown): obj is string => typeof obj === "string";
