import { urlHasProtocol } from "./urlHasProtocol.mjs";
import { querySelectorAll } from "./querySelectorAll.mjs";

/**
 * Makes sure urls to local pages are prepended with #/
 * @param container the element containing links to find
 */
export const rewriteLocalUrls = (container: ParentNode) => 
  querySelectorAll(container, "a").forEach((a) => {
    const href = a.getAttribute("href");
    if (href && !urlHasProtocol(href) && !href.startsWith("#")) {
      a.setAttribute("href", "#/" + href.replace(/^\.?\//, ""));
    }
  });
