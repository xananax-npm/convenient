/**
 * Mini-mustache templating system. Simply replaces all occurrences of {{key}} with the value of the key.
 */
export const print = (str: string, replacements: Record<string, any>) =>
	str.replace(/{{(.*?)}}/g, (_match, key) => replacements[key] || "");

export const makeStringTemplate = (
	str: string
): ((replacements: Record<string, any>) => string) => print.bind(null, str);

const htmlEscapeMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	"\x22": "&#x22;",
	"\x27": "&#x27;",
};
const escapeHTML = (string: string) =>
	("" + string).replace(
		/[&<>\'\"]/g,
		(x) => htmlEscapeMap[x as keyof typeof htmlEscapeMap]
	);


/**
 * Uses a similar system to https://johnresig.com/blog/javascript-micro-templating/ or  https://github.com/cho45/micro-template.js
 * DO NOT USE THIS WITH USER SUBMITTED DATA!
 * @param templateString 
 */
export const makeDynamicTemplate = (templateString: string) => {
	let line = 1;
	const templateFunctionBody = `
  const __line = 0
  const __ret = []
  const print = (...args) => __ret.push(...args)
  with(data){
    __ret.push(\`${
      templateString
          .replace(/[\r\t\n]/g, " ")
          .split("<%").join("\t")
          .replace(/((^|%>)[^\t]*)'/g, "$1\r")
          .replace(/\t=(.*?)%>/g, "`,$1,`")
          .split("\t").join("`);")
          .split("%>").join("__ret.push(`")
          .split("\r").join("\\'")
      + "`);}return __ret.join('');"
  }`;
	const func = new Function("data", templateFunctionBody) as (args:Record<string,unknown>) => string
	return func
}

