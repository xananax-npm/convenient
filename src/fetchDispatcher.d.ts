type Fetch = typeof fetch

interface FetchPatcher<T> extends Fetch {
  // tslint:disable-next-line:max-line-length
  (
    request?: string | Request,
    init?: (RequestInit & { invalidate?: boolean | undefined }) | undefined
  ): Promise<any>;
  onSuccess: (
    listener: FetchPatcherListener<FetchPatcherEventSuccess<T>>
  ) => () => void;
  onError: (
    listener: FetchPatcherListener<FetchPatcherEventError>
  ) => () => void;
  onLoad: (listener: FetchPatcherListener<FetchPatcherEventLoad>) => () => void;
  load: (request: Request, init?: RequestInit | undefined) => Promise<any>;
}

interface FetchPatcherEventLoad {
  type: "loading" | "success" | "error";
  request: Request;
}

interface FetchPatcherEventSuccess<T> extends FetchPatcherEventLoad {
  type: "success";
  request: Request;
  data: T;
}


interface FetchPatcherEventError<T = unknown> extends FetchPatcherEventLoad {
  type: "error";
  request: Request;
  data: T;
  error: Error;
}


type FetchPatcherEvent<T, E> =
  | FetchPatcherEventLoad
  | FetchPatcherEventError<E>
  | FetchPatcherEventSuccess<T>;


interface FetchPatcherListener<E extends FetchPatcherEvent<unknown>> {
  (event: E): void;
}
