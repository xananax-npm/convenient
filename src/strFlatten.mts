
/**
 * Utility to join a command together
 */
export const strFlatten =
  (...arrays: (string[] | string)[]) =>
  ( arrays
    .flat()
    .filter(Boolean)
    .join(' ')
  )