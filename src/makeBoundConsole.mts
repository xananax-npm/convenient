
type ConsoleProp = keyof Console

const methods: ConsoleProp[] = ["log", "warn", "error"];

/**
 * Returns console methods that can be used standalone (without requiring `console`).
 * Optional prefix will prepend every call with the provided prefix
 * @param prefix
 */
export function makeBoundConsole(prefix: string = "") {
  const [log, warn, error] = methods.map(
    (methodName) =>
      (...args: any[]) =>
        // @ts-expect-error (complains about ...args typing)
        console[methodName](prefix, ...args)
  );
  return { log, warn, error };
}
