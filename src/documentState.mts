import { createElementStatusModes } from './createElementStatusModes.mjs'

/**
 * Creates a document state object that can toggle between exclusive states.
 * All passed states' css classnames will be prepended with `mode-`.
 * @see {createElementStatusModes}
 * @param states
 */
export const documentState = (states: string[]) => {
  const all = states.map((state) => `mode-${state}`)

  return createElementStatusModes(all, document && document.body)
}
