//@ts-check

import { BREAK, type LOOP_SYMBOL } from "./symbols.mjs";


type PromiseMapper<T, O extends Record<string, unknown>, K extends keyof O> = (value: O[K], key: K, obj: O, stop: LOOP_SYMBOL) => T | undefined | LOOP_SYMBOL | Promise<T | undefined | LOOP_SYMBOL>




/**
 * Async property mapper which runs every property of an object
 * through a mapper function, and returns a new object with
 * mapped properties.
 * The mapper has the following signature:
 *
 * ```js
 * ( value, key, obj, stop ) => newValue | undefined | stop
 * ```
 *
 * if the mapper returns nothing (`undefined`) for a value, this
 * value will be skipped. If it returns the special `stop` value,
 * all subsequent values will be skipped
 *
 * If all keys are skipped, the returned object is `null`.
 */
export const promiseMap = <T, O extends Record<string, unknown>, K extends keyof O>(observer: PromiseMapper<T, O, K>, obj: O) => {
  /** @type {{ [P in K]?: T }} */
  const newValues: { [P in K]?: T; } = {};
  let oneKeyChanged = false;
  return Object.keys(obj)
    .map(
      (key: string) =>
        (previousReturnValue: unknown) =>
          previousReturnValue === BREAK
            ? Promise.resolve(BREAK)
            : Promise.resolve()
                .then(() =>
                  observer(
                    (obj[key] as O[K]),
                    (key as K),
                    obj,
                    BREAK
                  )
                )
                .then((response) => {
                  if (typeof response !== "undefined") {
                    if (response === BREAK) {
                      return BREAK;
                    }
                    oneKeyChanged = true;
                    newValues[key as K] = (response as T);
                  }
                  return true;
                })
    )
    .reduce((prev, curr) => prev.then(curr), Promise.resolve({}))
    .then(() => {
      if (oneKeyChanged) {
        return newValues;
      }
      return null;
    });
};
