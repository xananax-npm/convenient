//@ts-check
import { identityPromise, type PromisedIdentity } from "./identityPromise.mjs";

type MaybePromiseArity1<ARG, RESULT> = (arg: ARG) => RESULT | Promise<RESULT>;

type Promised<ARG, RESULT> = (arg: ARG) => Promise<RESULT>;

interface PromisePipeMaker {
	<A, B, C, D, E, F>(
		fn1: MaybePromiseArity1<A, B>,
		fn2: MaybePromiseArity1<B, C>,
		fn3: MaybePromiseArity1<C, D>,
		fn4: MaybePromiseArity1<D, E>,
		fn5: MaybePromiseArity1<E, F>
	): Promised<A, F>;
	<A, B, C, D, E>(
		fn1: MaybePromiseArity1<A, B>,
		fn2: MaybePromiseArity1<B, C>,
		fn3: MaybePromiseArity1<C, D>,
		fn4: MaybePromiseArity1<D, E>
	): Promised<A, E>;
	<A, B, C, D>(
		fn1: MaybePromiseArity1<A, B>,
		fn2: MaybePromiseArity1<B, C>,
		fn3: MaybePromiseArity1<C, D>
	): Promised<A, D>;
	<A, B, C>(
		fn1: MaybePromiseArity1<A, B>,
		fn2: MaybePromiseArity1<B, C>
	): Promised<A, C>;
	<A, B>(fn1: MaybePromiseArity1<A, B>): Promised<A, B>;
	<A, B>(
		...fns: [MaybePromiseArity1<A, any>, ...MaybePromiseArity1<any, any>[], MaybePromiseArity1<any, B>]
	): Promised<A, B>;
	(): PromisedIdentity;
}

/**
 * Chains functions together. Accepts promises and non-promises. For example:
 * ```js
 * const add = ( n:number ) => n+10
 * const multiply = ( n:number ) => n*100
 * const happy = ( n:string|number ) => `happy ${n}`
 * const birthday = ( s:string ) => `${s} birthday!`
 * const happyBirthday = pipe(add,multiply,happy,birthday)
 * happyBirthday(5).then(s=>console.log(s)) // happy 150 birthday!
 * ```
 */
export const promisePipe = (<A, B>(
	...fns: [Promised<A, any>, ...Promised<any, any>[], Promised<any, B>]
): Promised<A, B> => {
	switch (fns.length) {
		case 0:
			//@ts-ignore
			return identityPromise;
		case 1:
			return promisePipe1(fns[0]);
		case 2:
			return promisePipe2(fns[0], fns[1]);
		case 3:
			return promisePipe3(fns[0], fns[1], fns[2]);
		case 4:
			return promisePipe4(fns[0], fns[1], fns[2], fns[3]);
		case 5:
			return promisePipe5(fns[0], fns[1], fns[2], fns[3], fns[4]);
		default:
			break;
	}
	return promisePipes(...fns);
}) as PromisePipeMaker;

export const promisePipes =
	<ARG, RESULT>(
		...fns: [Promised<ARG, any>, ...Promised<any, any>[], Promised<any, RESULT>]
	): ((arg: ARG) => Promise<RESULT>) =>
	(arg) =>
		fns.reduce((p, fn) => p.then(fn), Promise.resolve(arg)) as Promise<RESULT>;

export const promisePipe1 =
	<A, B>(f: MaybePromiseArity1<A, B>): Promised<A, B> =>
	(a: A): Promise<B> =>
		Promise.resolve(a).then(f);

export const promisePipe2 =
	<A, B, C>(
		g: MaybePromiseArity1<A, B>,
		f: MaybePromiseArity1<B, C>
	): Promised<A, C> =>
	(a: A): Promise<C> =>
		Promise.resolve(a).then(g).then(f);

export const promisePipe3 =
	<A, B, C, D>(
		h: MaybePromiseArity1<A, B>,
		g: MaybePromiseArity1<B, C>,
		f: MaybePromiseArity1<C, D>
	): Promised<A, D> =>
	(a: A): Promise<D> =>
		Promise.resolve(a).then(h).then(g).then(f);

export const promisePipe4 =
	<A, B, C, D, E>(
		i: MaybePromiseArity1<A, B>,
		h: MaybePromiseArity1<B, C>,
		g: MaybePromiseArity1<C, D>,
		f: MaybePromiseArity1<D, E>
	): Promised<A, E> =>
	(a: A): Promise<E> =>
		Promise.resolve(a).then(i).then(h).then(g).then(f);

export const promisePipe5 =
	<A, B, C, D, E, F>(
		j: MaybePromiseArity1<A, B>,
		i: MaybePromiseArity1<B, C>,
		h: MaybePromiseArity1<C, D>,
		g: MaybePromiseArity1<D, E>,
		f: MaybePromiseArity1<E, F>
	): Promised<A, F> =>
	(a: A): Promise<F> =>
		Promise.resolve(a).then(j).then(i).then(h).then(g).then(f);
