//@ts-check
import { BREAK, SKIP, type LOOP_SYMBOL } from "./symbols.mjs";


type PromiseQueueMapper<T, R> = (input: T, index: number, arr: T[], stop: BREAK, skip: SKIP) => R | LOOP_SYMBOL | Promise<R | LOOP_SYMBOL>

interface PromiseQueueMaker{
	<T, R>(things:T[], map: PromiseQueueMapper<T, R>, skipErrors: false): Promise<(R | Error)[]>
	<T, R>(things:T[], map: PromiseQueueMapper<T, R>, skipErrors: true): Promise<R[]>
}


/**
 * Renders promises one by one, serially.
 * Each promise is passed to the map function.
 * If the map returns the special `SKIP` symbol, the current item is skipped
 * If the map returns the special `BREAK` symbol, next items are skipped
 * If `skipErrors` is true (default), promises that return an error are skipped.
 * Of course, if any of the above is true, the final array will not map exactly with the entry array
 * @param things an array of anything
 * @param map a map to submit the array to
 * @param [skipErrors] if true, errors will be skipped, otherwise, they will not
 */
export const promiseQueue = (<T, R>(things: T[], map: PromiseQueueMapper<T, R>, skipErrors = true): Promise<(R | Error)[]> => {
  const returnArr = [] as (R | Error)[];
  return things
    .reduce(
      (prev: Promise<unknown>, item: T, index) => {
        return prev.then((isStop) =>
          isStop === BREAK
            ? Promise.resolve(BREAK)
            : Promise.resolve()
                .then(() => map(item, index, things, BREAK, SKIP))
                .then((data) =>
                  data === BREAK
                    ? BREAK
                    : data === SKIP
                    ? undefined
                    : (returnArr[index] = data)
                )
                .catch((error: Error) =>
                  !skipErrors ? (returnArr[index] = error) : undefined
                )
        );
      },
      Promise.resolve()
    )
    .then(() => returnArr.filter(Boolean));
}) as PromiseQueueMaker;
