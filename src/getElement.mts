import { isElement } from "./isElement.mjs";

/**
 * Little utility so people can pass css selectors or elements in initialization
 * options.
 * A minimal replacement to a more full fledged selector engine like jQuery
 * @param elementOrString
 */
export const getElement = (elementOrString: string | HTMLElement) => {
  const element =
    elementOrString && typeof elementOrString === "string"
      ? document.querySelector(elementOrString) as HTMLElement
      : elementOrString;
  return isElement(element) ? element : null;
};
