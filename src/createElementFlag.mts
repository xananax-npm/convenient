//@ts-check
/**
 * Creates a helper to add or remove global classes that begin with `is-`.
 * This is similar to @see {@link createElementStatusModes}, but while status modes are exclusives, 
 * flags can be cumulative (use this function multiple times).
 * @param name name of the state
 * @param [element] defaults to the document body
 */
export const createElementFlag = (name: string, element: Element = document.body) => {
  return {
    on: () => element.classList.add(`is-${name}`),
    off: () => element.classList.remove(`is-${name}`),
    toggle: () => element.classList.toggle(`is-${name}`),
    has: () => element.classList.contains(`is-${name}`),
  };
}
