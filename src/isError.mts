
/**
 *
 * Returns true if the passed object is an Error
 */
export const isError = (thing: unknown): thing is Error => thing != null && thing instanceof Error;
