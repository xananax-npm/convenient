import { isPlainObject } from "./isPlainObject.mjs";
import { isArrayLike } from "./isArrayLike.mjs";

export type Iteratable<T> = T[] | { [key: string]: T }
export type MapFunction<T, V> = (value: T, key: string | number, obj: Iteratable<T>) => V




export function iterate<T, V, O extends Iteratable<T>>(obj: O, map: MapFunction<T, V>): V[] {
  const keys = isPlainObject(obj)
    ? Object.keys(obj)
    : isArrayLike(obj)
    ? obj
    : [];

  return keys.map((k: string | number, _i) =>
		//@ts-ignore
    map(obj[k], k, obj)
  );
}
