type JsonObject =
  | string
  | number
  | boolean
  | null
  | JsonObject[]
  | { [key: string]: JsonObject }

type JsonRoot = { [key: string]: JsonObject }
