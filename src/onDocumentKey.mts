
export const onDocumentKeyUp = (keyToListen: string, keyCallback: () => void) =>
  document.addEventListener(
    "keyup",
    ({ key }) => key === keyToListen && keyCallback()
  );


export const onDocumentKeyDown = (keyToListen: string, keyCallback: () => void) =>
  document.addEventListener(
    "keydown",
    ({ key }) => key === keyToListen && keyCallback()
  );


export const onDocumentKey = (keyToListen: string, keyCallback: (isDown: boolean) => void) => {
  document.addEventListener(
    "keyup",
    ({ key }) => key === keyToListen && keyCallback(false)
  );
  document.addEventListener(
    "keydown",
    ({ key }) => key === keyToListen && keyCallback(true)
  );
};
