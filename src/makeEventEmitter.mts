
/**
 * Base type that a CustomEventEmitter may receive as a generic type
 * to discriminate possible events.
*/
type CustomEventMap = Record<string, unknown>

/**
 * Extracts the keys from a CustomEventMap so they can be used as event types.
 * For example, for the CustomEventMap
 * ```ts
 * {
 *   "longpress": {position: number[]}
 *   "shortpress": {position: number[]}
 * }
 * ```
 * This type will infer `"longpress" | "shortpress"`.
 * 
 */
type CustomEventKey<EvtMap extends CustomEventMap> = string & keyof EvtMap

/**
 * Any function or object that can be used to listen to an event.
 */
type CustomEventListenerOrEventListenerObject<T> = | { handleEvent(event: CustomEvent<T>): void }| ((event: CustomEvent<T>) => void)

/**
 * An event emitter that can be mixed in to other objects.
 */
interface CustomEventEmitter<EvtMap extends CustomEventMap>{
	addEventListener: <K extends CustomEventKey<EvtMap>>(eventName: K, fn: CustomEventListenerOrEventListenerObject<EvtMap[K]>, options?: AddEventListenerOptions | boolean) => void
	removeEventListener: <K extends CustomEventKey<EvtMap>>(eventName: K, fn: CustomEventListenerOrEventListenerObject<EvtMap[K]>, options?: EventListenerOptions | boolean) => void
	dispatchEvent: <K extends CustomEventKey<EvtMap>>(eventName: K, detail: EvtMap[K]) => void
	signal: AbortSignal
	abort: AbortController["abort"]
}

/**
 * Returns a native browser event target that is properly typed.
 * Three major differences with a classical event target:
 *
 * 1. The emitter's methods are bound and can be passed to other objects
 * 2. The emitter has an `abort` property and a `signal` property that can be
 *    used to abort all listeners (you have to explicitely pass it though, it's
 *    not automatic)
 * 3. `dispatchEvent` has a different signature `(type, event)` rather than just
 *     `event`. This is because there is no way to enforce a string & details
 *     tuple on a CustomEvent using Typescript or JSDocs.
 * 
 */
export function makeEventEmitter<EvtMap extends CustomEventMap>(): CustomEventEmitter<EvtMap> {
  let abortController = new AbortController();
  const eventEmitter = new EventTarget();
  const addEventListener = eventEmitter.addEventListener.bind(eventEmitter);
  const removeEventListener =
    eventEmitter.removeEventListener.bind(eventEmitter);

  /**
   * Dispatches a custom event to all listeners of that event.
   * @type {CustomEventEmitter<EvtMap>["dispatchEvent"]}
   */
  const dispatchEvent: CustomEventEmitter<EvtMap>["dispatchEvent"] = (
    type,
    detail
  ) => {
    const event = new CustomEvent(type, { detail });
    eventEmitter.dispatchEvent(event);
  };

  /**
   * Aborts any eventListener, fetch, or other process that received the signal.
   * resets the abort controller and signal (they are new instances)
   */
  const abort = (reason: any) => {
    abortController.abort(reason);
    abortController = new AbortController();
  };

  return {
    dispatchEvent,
    addEventListener: (eventName, fn, options) => {
      return addEventListener(
        eventName,
        fn as EventListenerOrEventListenerObject,
        options
      );
    },
    removeEventListener: (eventName, fn, options) => {
      return removeEventListener(
        eventName,
        fn as EventListenerOrEventListenerObject,
        options
      );
    },
    abort,
    get signal() {
      return abortController.signal;
    },
  };
}
