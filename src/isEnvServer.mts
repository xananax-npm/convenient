//@ts-check
import { isEnvBrowser } from './isEnvBrowser.mjs'
/**
 * is true if the environment is not browser
 */
export const isEnvServer = !isEnvBrowser

