
export type PascalCasedString = string

/**
 *
 * Turns a string into a PascalCased string.
 *
 * If passed an array, it will be joined with the `sep` argument.
 *
 * @param str
 * @param [sep] defaults to `_`
 */
export const toPascalCase = (str: (string | string[]), sep: string = "_"): PascalCasedString => {
  if(Array.isArray(str)){
    return str.map((s) => toPascalCase(s, sep)).join(sep)
  }

  if (/^[a-z\d]+$/i.test(str)) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  return str.replace(
    /([a-z\d])([a-z\d]*)/gi,
    (g0, g1, g2) => g1.toUpperCase() + g2.toLowerCase()
  ).replace(/[^a-z\d]/gi, '');
}
