/**
 * Escapes a string so it can be used in a regular expression
 * @param text
 */
export const escapeRegExp = (text: string) => 
  text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");

