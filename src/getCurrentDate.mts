export type YYYYMMDD =
	`${number}${number}${number}${number}-${number}${number}-${number}${number}`;

export type HHMMSS = `${number}${number}:${number}${number}:${number}${number}`;

/**
 * Returns current date
 */
export const getCurrentDate = () => {
	let _dateTime = new Date();
	const offset = _dateTime.getTimezoneOffset();
	_dateTime = new Date(_dateTime.getTime() - offset * 60 * 1000);
	const _dateTimeArr = _dateTime.toISOString().split("T");
	const unix = _dateTime.getTime();
	const date = _dateTimeArr[0] as YYYYMMDD;
	const time = _dateTimeArr[1].replace(/Z$/,'') as HHMMSS;
	return { date, time, unix };
};
