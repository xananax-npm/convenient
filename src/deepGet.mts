//@ts-check

/**
 * Returns a deeply nested value in an object. Can use a wildcard to return a collection
 * Inspired by https://github.com/developit/dlv
 * @template T
 * @param obj an object to search into
 * @param path a path, as a dot delimited string or an array of string
 * @param [defaultValue] a value to return if nothing was found
 * @param [separator]
 */
export const deepGet = <T,>(obj: any, path: string | string[], defaultValue?: T, separator: string = ".") => {
  if (!obj) {
    throw new Error("no object provided");
  }
  path = typeof path === "string" ? path.split(separator) : path;
  return __deep_get(obj, path, defaultValue);
}

const __deep_get = <T,>(obj: any, path: string[], defaultValue: T): T => {
  const { length } = path;
  let p = 0;
  while (obj && p < length) {
    const key = path[p++];
    const val = obj[key];
    if (Array.isArray(val) && path[p] === "*") {
      {
        const rest = path.slice(p + 1);
        p = length;
        obj = val.map((item) => __deep_get(item, rest, defaultValue));
      }
    } else {
      obj = (val);
    }
  }
  const notFound = obj === undefined || p < length;
  return notFound ? defaultValue : obj;
};
