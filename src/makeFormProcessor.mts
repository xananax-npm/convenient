//@ts-check
import type { SerializedForm } from "./serializeForm.mjs";

type Transformer = (serialized: SerializedForm) => SerializedForm | undefined | Promise<SerializedForm | undefined>

type ValidatorErrorsDictionary = Record<string, string>

type Validator = (serialized: SerializedForm, errors: ValidatorErrorsDictionary) => void

type ValidatedForm = SerializedForm & {errors?: ValidatorErrorsDictionary}


export const transformForm =
  (transform?: Transformer) => (serialized: SerializedForm) =>
    transform
      ? Promise.resolve()
          .then(() => transform(serialized))
          .then((retValue) =>
            retValue ? { ...serialized, ...retValue } : serialized
          )
      : Promise.resolve(serialized);

export const validateForm =
  (validate?: Validator) =>
  (serialized: SerializedForm, errors: ValidatorErrorsDictionary = {}): Promise<ValidatedForm> =>
    validate
      ? Promise.resolve()
          .then(() => {
            validate(serialized, errors);
          })
          .then(() =>
            Object.keys(errors).length > 0
              ? { ...serialized, errors }
              : serialized
          )
      : Promise.resolve(serialized);

/**
 * Returns a function that takes a serialized form and puts
 * it through a transformer function, then a validator function
 *
 * A serialized form looks like so
 * ```
 * { name: string
 *   action: string
 *   method: string
 *   values: SerializedFormValues{
 *    [key: string]: InputValue | SerializedFormValues | (InputValue | SerializedFormValues)[]
 *   }
 *   enctype: string
 *   target: string
 * }
 * ```
 * Where `InputValue` can be `string | number | boolean | File | Date | File[]`
 *
 * The transformer has the signature
 * `(serialized):serialized`
 *
 * If an object is received, it will be merged with the original serialized form
 *
 * The validator has the signature
 * `(serialized, errors):void`
 * the `errors` object is empty. It's up to the function to fill it with {key:string}
 *
 * Returns a processor function which receives the serialized form, and returns a promise
 * @param {{ validate?: Validator; transform?: Transformer }} [properties] an object with { validate, transform } both are optional
 */
export const makeFormProcessor = ({ validate, transform }: { validate?: Validator; transform?: Transformer; } = {}) => {
  const _validate = validateForm(validate);
  const _transform = transformForm(transform);
  const process = (/** @type {SerializedForm} */ serialized: SerializedForm) =>
    Promise.resolve(serialized).then(_transform).then(_validate);
  return process;
};
