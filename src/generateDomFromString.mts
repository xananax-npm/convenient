
/**
 * Generates valid dom elements from a string
 */
export const generateDomFromString = (htmlString: string): DocumentFragment => {
  htmlString = htmlString.trim();
  const dom = new DOMParser().parseFromString(
    "<template>" + htmlString + "</template>",
    "text/html"
  );
  const content = (
    dom?.head?.firstElementChild as HTMLTemplateElement
  )?.content;

  const fragment = document.createDocumentFragment();
  fragment.append(content);

  return fragment;
}

/**
 * @param html representing a single element
 */
export function htmlToElement(html: string): Element | null {
  html = html.trim();
  const template = document.createElement("template");
  template.innerHTML = html;
  return (template.content.firstChild as Element);
}

/**
 * @param html representing any number of sibling elements
 */
export function htmlToElements(html: string): NodeList {
  const template = document.createElement("template");
  template.innerHTML = html;
  return template.content.childNodes;
}
