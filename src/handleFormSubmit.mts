//@ts-check
import { serializeForm , type SerializedForm} from './serializeForm.mjs'

type FormHandler = (serialized: SerializedForm, form: HTMLFormElement) => void



/**
 * Creates a function that handles submit events.
 * The function:
 * - stops the event from propagating
 * - serializes the form inputs
 * - runs the provided callback with the serialized form
 */
export const handleFormSubmit =
  (callback: FormHandler) => (event: Event) => {
    event.preventDefault()
    event.stopPropagation()
    const form = event.target as HTMLFormElement
    callback(serializeForm(form), form)
  }

