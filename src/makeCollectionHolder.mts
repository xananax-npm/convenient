import { makeStateHolder } from "./makeStateHolder.mjs";
import { SKIP, BREAK, type LOOP_SYMBOL } from "./symbols.mjs";


/**
 * Creates a collection that sends events when it changes
 * @param [initialCollection] a collection to start with
 */
export const make_collection_holder = <T,>(initialCollection: T[] = []) => {
  const { connect, setState, clean, getState } =
    makeStateHolder(initialCollection);

  /**
   * Appends an item to the collection
   * @param item the item to append
   */
  const add = (item: T) => {
    const newCollection = [...getState(), item];
    setState(newCollection);
  };

  /**
   * Removes an item from the collection
   * @param item the item to remove
   */
  const remove = (item: T) => {
    const index = getState().indexOf(item);
    if (index >= 0) {
      const newCollection = getState().slice();
      newCollection.splice(index, 1);
      setState(newCollection);
    }
  };

  /**
   * Updates an item in the collection
   * @param item the item to update
   * @param partialItem a set of properties to update in the item
   */
  const update = (item: T, partialItem: Partial<T>) => {
    const index = getState().indexOf(item);
    if (index >= 0) {
      const newItem = { ...item, ...partialItem };
      const newCollection = getState().slice();
      newCollection.splice(index, 1, newItem);
      setState(newCollection);
    }
  };

  /**
   * Maps the collection.
   * This can also be used to filter, or to find.
   *
   * Return `SKIP` to filter an item out
   * Return `BREAK` to stop the loop
   *
   * `SKIP` and `BREAK` can be imported from the module, or
   * from the collectionHolder itself
   * @param {(item: T, index: number, arr: T[]) => S | LOOP_SYMBOL} fn the function to use. Return SKIP to skip a member, BREAK to stop processing
   */
  const map = <S,>(fn: (item: T, index: number, arr: T[]) => S | LOOP_SYMBOL) => {
    const output: S[] = [];
    let i = 0;
    const collection = getState();
    const { length } = collection;
    while (i < length) {
      const item = collection[i];
      const ret = fn(item, i, collection);
      i = i + 1;
      if (ret === SKIP) {
        continue;
      }
      if (ret === BREAK) {
        return output;
      }
      output.push(ret);
    }
    return output;
  };

  /**
   * Returns the first item on which the given predicate returns true
   * @param fn the function to run on each member
   */
  const find = (fn: (item: T, index: number, arr: T[]) => boolean) => {
    let i = 0;
    const collection = getState();
    const { length } = collection;
    while (i < length) {
      const item = collection[i];
      const ret = fn(item, i, collection);
      if (ret) {
        return item;
      }
      i = i + 1;
    }
    return null;
  };

  /**
   * Returns true if at least one item from the collection satisfies the predicate
   * @param fn the predicate
   */
  const some = (fn: (item: T, index: number, arr: T[]) => boolean) => find(fn) !== null;

  /**
   * Returns true if no item from the collection satisfies the predicate
   * @param fn the predicate
   */
  const none = (fn: (item: T, index: number, arr: T[]) => boolean) => find(fn) === null;

  /**
   * Returns the current size of the collection
   */
  const size = () => getState().length;

  /**
   * Returns the item at the specified index
   * Throws if the index is invalid or if no item is set
   * @param index the item's index
   */
  const getByIndex = (index: number) => {
    const item = getState()[index];
    if (typeof item === "undefined") {
      throw new Error("Accessing an invalid index");
    }
    return item;
  };

  return {
    some,
    none,
    size,
    find,
    map,
    update,
    remove,
    add,
    getByIndex,
    connect,
    setState,
    clean,
    getState,
    BREAK,
    SKIP,
  };
};

export const makeCollectionHolder = make_collection_holder;
