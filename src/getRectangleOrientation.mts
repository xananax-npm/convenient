//@ts-check
import { isNumeric } from "./isNumeric.mjs";

export type Orientation = 'portrait' | 'landscape' | 'square' | 'unknown'


export const ORIENTATION_PORTRAIT = "portrait" as Orientation;
export const ORIENTATION_LANDSCAPE = "landscape" as Orientation;
export const ORIENTATION_SQUARE = "square" as Orientation;
export const ORIENTATION_UNKNOWN = "unknown" as Orientation;

/**
 * Returns an orientation string for a given width and height
 * @param width
 * @param height
 */
export const getRectangleOrientation = (width: number | string, height: number | string) =>
  width && height && isNumeric(width) && isNumeric(height)
    ? width === height
      ? ORIENTATION_SQUARE
      : height > width
      ? ORIENTATION_PORTRAIT
      : ORIENTATION_LANDSCAPE
    : ORIENTATION_UNKNOWN;

export const getImageOrientation = getRectangleOrientation;
