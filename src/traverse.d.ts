type SKIP = import('./symbols.mjs').SKIP
type BREAK = import('./symbols.mjs').BREAK

type TraversableKey<T> = T extends Array<any> | string
  ? number
  : T extends Map<infer K1, unknown>
  ? K1
  : T extends Set<infer V>
  ? V
  : string | number | symbol;

type TraversableValue<T> = T extends Array<infer V1>
  ? V1
  : T extends string
  ? string
  : T extends Map<unknown, infer V1>
  ? V1
  : T extends Set<infer V1>
  ? V1
  : T extends Record<string | number | symbol, infer V1>
  ? V1
  : never;

interface TraversableObserver<T, K, V> {
  (value: V, key: K, source: T, BREAK: BREAK): void | BREAK;
}

type TraversableFunctor<VALUE, DESTINATION> = (
  destination: DESTINATION,
  value: VALUE,
  key: string | number | symbol,
  source: { [key: string]: VALUE } | VALUE[],
  skip: SKIP,
  BREAK: BREAK
) => DESTINATION | BREAK | SKIP


type TraversableMapper<VALUE, TRANSFORMED> = (
  value: VALUE,
  key: string | number | symbol,
  source: { [key: string]: VALUE } | VALUE[],
  skip: SKIP,
  BREAK: BREAK
) => TRANSFORMED | BREAK | SKIP