//@ts-check
/**
 * is true if the environment is production
 */
export const isProd =
  typeof process !== 'undefined' &&
  process.env &&
  process.env.NODE_ENV === 'production'


