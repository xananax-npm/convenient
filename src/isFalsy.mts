export type Falsy = undefined | null | '' | [] | false

/**
 * Returns true if the object passed is falsy.
 *
 * Considered falsy:
 * undefined, null, empty strings, empty arrays, objects with no keys
 *
 * the number "0" is *not* considered falsy.
 *
 */
export const isFalsy = (thing: unknown): thing is Falsy =>
  typeof thing === 'number'
    ? false
    : typeof thing === 'boolean'
    ? !thing
    : typeof thing === 'undefined' ||
      thing === null ||
      thing === '' ||
      ((typeof thing === 'string' || (typeof thing === 'object' && 'length' in thing)) && !thing.length) ||
      Object.keys(thing).length === 0
