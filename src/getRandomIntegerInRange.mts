
/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * @param max
 * @param [min]
 * @param [rand] defaults to javascript's `Math.random`
 */
export const getRandomIntegerInRange = (max: number, min: number = 0, rand: () => number = Math.random) =>
  Math.floor(rand() * (max - min + 1)) + min;
