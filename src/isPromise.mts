import { isThenable } from './isThenable.mjs'
import { isFunction } from './isFunction.mjs'

/**
 * Returns true if the passed object has a `then` method
 * and a `catch` method
 *
 */
export const isPromise = (thing: unknown): thing is Promise<unknown> =>
  isThenable(thing) &&
  'catch' in thing &&
  isFunction(
    /** @type {PromiseLike<unknown> & { catch: (err?: unknown) => boolean }} */(thing)
      .catch
  )

