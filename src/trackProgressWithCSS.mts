import { percentFromProgress } from "./percentFromProgress.mjs";

export interface CSSProgressData{
	received: number,
	total: number
}

export interface ProgressSetter{
	(data: CSSProgressData): void
}


/**
 * Returns a function that can update an HTML Element.
 * The function, when called with `received` and `total`, set 3 custom css vars
 * on the element, which can be used in CSS to reflect the state of the object
 * @param element
 * @param [prefix]
 */
export const trackProgressWithCSS = (element: HTMLElement, prefix = "load-") => {
  const keyFract = `--${prefix}fraction`;
  const keyProgress = `--${prefix}progress`;
  const keyProgressStr = `--${prefix}progress-str`;
  const classComplete = `${prefix}complete`;
  const setProgress:ProgressSetter = ({ received, total }) => {
    const final = received === total;
    const fraction = final ? 1 : received / total;
    const percent = final ? "100%" : percentFromProgress(fraction, true);
    console.log(keyProgress, percent);
    element.style.setProperty(keyFract, `${fraction}`);
    element.style.setProperty(keyProgress, percent);
    element.style.setProperty(keyProgressStr, `"${percent}"`);
    if (final) {
      console.log("all done!", element, classComplete);
      requestAnimationFrame(() => {
        element.classList.add(classComplete);
      });
    }
  };
  return setProgress;
};
