import { decodeContentLength } from './decodeContentLength.mjs'
import { fetchHeaders } from './fetchHeaders.mjs'

/**
 * Attempts to retrieve the size of an object represented by a URL with a
 * limited fetch request.
 * @see {decodeContentLength}
 * @param path
 */
export const fetchContentLength = async (path: string): Promise<number> => 
  decodeContentLength(
    await fetchHeaders(path)
  )

