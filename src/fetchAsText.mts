import { isLocalhost } from "./isLocalhost.mjs";

/**
 * Loads a text file. The path provided will be appended with a random number
 * when running on localhost to avoid caching problems
 * @param path
 * @returns the loaded file
 */
export const fetchAsText = (path: string): Promise<string> =>
  fetch(isLocalhost ? `./${path}?rand=${Math.random()}` : `./${path}`).then(
    (response) => response.text()
  );
