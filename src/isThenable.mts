import { isFunction } from "./isFunction.mjs";

/**
 * Returns true if the passed object has a `then` method
 *
 */
export const isThenable = (thing: unknown): thing is PromiseLike<unknown> =>
  thing != null &&
  (typeof thing === "object" || typeof thing === "function") &&
  "then" in thing &&
  isFunction(thing.then);
