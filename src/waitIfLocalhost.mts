import { wait } from "./wait.mjs";
import { identityPromise, type PromisedIdentity } from "./identityPromise.mjs";
import { isLocalhost } from "./isLocalhost.mjs";


/**
 * Useful to check for transitions while developing styles, if the loading screen
 * disappears too fast for example.
 * @param [durationMs] Duration, in milliseconds. Defaults to 1 second
 */
export const waitIfLocalhost =
  ( durationMs = 1_000 ): PromisedIdentity =>
    isLocalhost 
    ? wait(durationMs) 
    : identityPromise
    ;